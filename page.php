<?php get_header(); ?>
	<header class="page-header">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 col-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</header>
	<div id="main" class="container">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="row justify-content-center">
			<div class="col-lg-8 col-12">
				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<section class="post_content" itemprop="articleBody">
							<?php the_content(); ?>
						</section> <!-- end article section -->
					</article> <!-- end article -->

					</div>
		</div>
				<?php endwhile; ?>
				<?php else : ?>
					<article id="post-not-found">
						<header>
							<h1><?php _e("Not Found", "bonestheme"); ?></h1>
						</header>
						<section class="post_content">
							<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
						</section>
					</article>



		<?php endif; ?>
	</div> <!-- end #main -->

<script>
jQuery(function(){
	jQuery('a[href^="#"').on('click',function(e){
		if(!jQuery(this).hasClass('back-top')){
			e.preventDefault();
			var location = jQuery(this.hash).offset().top - 120;
			jQuery('html,body').animate({scrollTop: location}, 500);
		}
	});
});
</script>

<?php get_footer(); ?>
