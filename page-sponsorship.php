<?php
/*
Template Name: Sponsorship Page
*/
?>

<?php get_header(); ?>






					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<header>

							<div class="page-header"><h1><?php the_title(); ?></h1></div>

						</header> <!-- end article header -->

					<div class="container">
						<div class="row justify-content-center">
							<div class="col-md-12 col-lg-8 col-12">
								<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
									<section class="post_content">
										<?php the_content(); ?>

										<p>Purchase a sponsorship online and pay with PayPal (you don’t have to have a PayPal account).</p>
										<?php echo do_shortcode("[gravityform id='5' name='Luncheon Sponsorship' title='false' description='false' ajax='true']"); ?>

									</section> <!-- end article section -->
								</article> <!-- end article -->
							</div>
						</div>
					</div>

					<?php endwhile; ?>

					<?php else : ?>

					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>

					<?php endif; ?>


<?php get_footer(); ?>
