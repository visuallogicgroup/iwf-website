

<footer class="footer">
	<img class="circles" src="<?php echo get_template_directory_uri(); ?>/images/circles.svg" alt="">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<h2>SUPPORT US</h2>
				<p class="mb-0">Make a contribution today</p>
				<a href="<?php echo home_url(); ?>/donate-now" class="btn btn-primary mb-5">DONATE</a>
				<p class="mt-5 mb-0">Help us support women and girls</p>
				<a href="<?php echo home_url(); ?>/your-support/volunteer" class="btn btn-primary mb-5">VOLUNTEER</a>
			</div>
			<div class="col-sm-4">
				<h2>CONTACT US</h2>
				<p>The Iowa Women's Foundation <br> 2201 East Grantview Dr., Suite 200 <br> Coralville, IA 52241</p>
				<p><a href="tel:319-774-3813">319-774-3813</a><br>
				<a href="mailto:info@iawf.org">info@iawf.org</a></p>
				<ul class="social">
					<li>
						<a href="http://www.twitter.com/IowaWomensFdn" target="_blank" title="Visit Us On Twitter"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-twitter.svg" alt=""></a>
					</li>
					<li>
						<a href="http://facebook.com/IowaWomensFoundation" target="_blank" title="Visit Us On Facebook"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-facebook.svg" alt=""></a>
					</li>
				</ul>
			</div>
			<div class="col-sm-4">
				<h2>QUICK LINKS</h2>
				<?php
    wp_nav_menu( array(
      'theme_location'  => 'footer_links',
      'depth'	          => 1, // 1 = no dropdowns, 2 = with dropdowns.
      'container'       => '',
      'container_class' => '',
      'container_id'    => '',
      'menu_class'      => 'quick-links-list',
      'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
      'walker'          => new WP_Bootstrap_Navwalker(),
    ) ); ?>

			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<hr>
			</div>
		</div>
		<div class="row disclaimers">
			<div class="col-md-3 col-12">
				<p>&copy; 2019 Iowa Women's Foundation</p>
			</div>
			<div class="col-md-6 col-12"></div>
			<div class="col-md-3 col-12 text-md-right">
				<p>Experience by <a href="https://vlgux.com" target="_blank">Visual Logic</a></p>
			</div>
		</div>
	</div>
</footer>


			<a href="#top" class="back-top hide" data-toggle="tooltip" title="Back to Top"><i class="icon-chevron-up"></i></a>





		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->

		<?php wp_footer(); // js scripts are inserted using this function ?>

	<script>
	jQuery(function() {
    //caches a jQuery object containing the header element
    var header = jQuery(".headerTop");
    jQuery(window).scroll(function() {
        var scroll = jQuery(window).scrollTop();

        if (scroll >= 70) {
            header.removeClass('headerTop').addClass("headerScrolled");
        } else {
            header.removeClass("headerScrolled").addClass('headerTop');
        }
    });
});

jQuery(function() {
    //caches a jQuery object containing the header element
    var backToTop = jQuery(".back-top");
    jQuery(window).scroll(function() {
        var scroll = jQuery(window).scrollTop();

        if (scroll >= 200) {
            backToTop.removeClass('hide');
        } else {
            backToTop.addClass('hide');
        }
    });
});


	</script>

	</body>

</html>
