<?php
/*
Template Name: Home
MultiEdit: Left,Right
*/
?>

<?php get_header(); ?>

<header class="header">
<div class="carousel-wrap">
<div id="carouselMain" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
    <li data-target="#carouselMain" data-slide-to="0" class="active"></li>
    <li data-target="#carouselMain" data-slide-to="1"></li>
    <li data-target="#carouselMain" data-slide-to="2"></li>
    <li data-target="#carouselMain" data-slide-to="3"></li>
  </ol>
		<div class="carousel-inner">

      <div class="carousel-item active" data-interval="10000">
				<div  class="slide-background" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/bg-careforce.jpg'), linear-gradient(-254deg, #db9c27 0%,#b82526 50%,#7b2d67 100%);">
					<div class="container-fluid header-content">
						<div class="row justify-content-md-center flex-md-column align-items-center">
							<div class="col-lg-7 col-md-9 text-center">
                <img src="/wp-content/themes/iwf-website/images/logo-careforce-white.png">
								<p class="mt-5" style="font-size:2rem;">Support Your Child Care Professionals.</p>
                <a href="/careforce/" class="btn btn-primary">Learn More</a>
							</div>
						</div>

					</div>
				</div>

			</div>

			<div class="carousel-item" data-interval="10000">
				<div  class="slide-background" style="background-position: <?php echo get_field('hero_slider_image_1_position'); ?>; background-image: url('<?php echo get_field('hero_slider_image_1'); ?>'), linear-gradient(-254deg, #db9c27 0%,#b82526 50%,#7b2d67 100%);">
					<div class="container-fluid header-content">
						<div class="row justify-content-md-center flex-md-column align-items-center">
							<div class="col-lg-7 col-md-9 text-center">
								<a class="" data-toggle="modal" data-target="#videoModal"><img class="play-button" src="<?php echo get_template_directory_uri(); ?>/images/icon-play-button.svg"></a>
								<h1 class="my-4"><?php echo get_field('hero_slider_h1_1'); ?></h1>
								<p><?php echo get_field('hero_slider_content_1'); ?></p>
							</div>
						</div>

					</div>
				</div>

			</div>


			<div class="carousel-item">
				<div class="slide-background" style="background-position: <?php echo get_field('hero_slider_image_2_position'); ?>; background-image: url('<?php echo get_field('hero_slider_image_2'); ?>'), linear-gradient(-254deg, #db9c27 0%,#b82526 50%,#7b2d67 100%);">
				<div class="container-fluid header-content">
						<div class="row justify-content-md-end align-items-center">
							<div class="col-md-5">
								<h1 class="my-4"><?php echo get_field('hero_slider_h1_2'); ?></h1>
								<p><?php echo get_field('hero_slider_content_2'); ?></p>

							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="carousel-item">
				<div class="slide-background" style="background-position: <?php echo get_field('hero_slider_image_3_position'); ?>; background-image: url('<?php echo get_field('hero_slider_image_3'); ?>'), linear-gradient(-254deg, #db9c27 0%,#b82526 50%,#7b2d67 100%);">
				<div class="container-fluid header-content">
						<div class="row justify-content-md-end flex-md-column align-items-center">
							<div class="col-lg-7 col-md-9 d-md-flex flex-md-column justify-content-md-end text-md-center">

								<h1 class="my-4"><?php echo get_field('hero_slider_h1_3'); ?></h1>
								<p><?php echo get_field('hero_slider_content_3'); ?></p>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>



</header>
<?php if (get_field('custom_alert_message')) { ?>
	<div class="banner-alert">
	<p><?php echo get_field('custom_alert_message'); ?></p>
</div>
<?php } ?>


<div class="container-fluid py-5 vlg-my-5">
	<div class="row justify-content-center">
		<div class="col-xl-6 col-lg-8 col-md-10 text-md-center">
			<h2>OUR MISSION</h2>
			<h1><?php echo get_field('our_mission_sub_heading'); ?></h1>
			<p class="my-5"><?php echo get_field('our_mission_paragraph'); ?></p>
				<a href="<?php echo get_home_url(); ?><?php echo get_field('our_mission_cta_link'); ?>" class="btn btn-primary my-3"><?php echo get_field('our_mission_cta_button'); ?></a>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row recent-posts align-items-stretch">

		<?php $usedIDs = array(); ?>
		<div class="col-12 col-lg-3 col-no-pad">
			<?php $covidQuery = new WP_Query( array( 'category_name' => 'covid-19', 'posts_per_page' => 1 ) ); ?>

			<?php if ($covidQuery->have_posts()) : while ($covidQuery->have_posts()) : $covidQuery->the_post(); ?>
				<a href="<?php echo the_permalink();?>">
					<div class="recent-post-background bg-purple" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/stand-together.jpg'">
							<div class="recent-post-content">
							<?php array_push($usedIDs, get_the_ID()); ?>
								<h2>WE STAND TOGETHER</h2>
								<h1><?php echo the_title(); ?></h1>
								<p class="gold-text"><?php echo get_the_date(); ?></p>
							</div> <!-- END recent-post-content -->
						</div> <!-- END recent-post-background -->
					</a>

					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif;?>
		</div> <!-- END col col-lg-4 -->

    <?php $usedIDs = array(); ?>
		<div class="col-12 col-lg-3 col-no-pad">
			<?php $query = new WP_Query( array( 'category_name' => 'news', 'posts_per_page' => 1 ) ); ?>

			<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
				<a href="<?php echo the_permalink();?>">
					<div class="recent-post-background" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/handshake-grey.jpg'">
							<div class="recent-post-content">
							<?php array_push($usedIDs, get_the_ID()); ?>
								<h2>RECENT NEWS</h2>
								<h1><?php echo the_title(); ?></h1>
								<p class="gold-text"><?php echo get_the_date(); ?></p>
							</div> <!-- END recent-post-content -->
						</div> <!-- END recent-post-background -->
					</a>

					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif;?>
		</div> <!-- END col col-lg-4 -->

		<!-- store events ID in the array before the middle block renders -->
		<?php $events_query = new WP_Query( array( 'category_name' => 'events', 'posts_per_page' => 1 ) ); ?>
		<?php if ($events_query->have_posts()) : while ($events_query->have_posts()) : $events_query->the_post(); ?>
		<?php array_push($usedIDs, get_the_ID()); ?>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
		<?php endif;?>

    <?php $usedIDs = array(); ?>
		<div class="col-12 col-lg-3 col-no-pad">
			<?php $query = new WP_Query( array( 'category_name' => 'uncategorized', 'posts_per_page' => 1 ) ); ?>

			<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
				<a href="<?php echo the_permalink();?>">
					<div class="recent-post-background bg-purple" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/community-hands-grey.jpg'">
							<div class="recent-post-content">
							<?php array_push($usedIDs, get_the_ID()); ?>
								<h2>RECENT POST</h2>
								<h1><?php echo the_title(); ?></h1>
								<p class="gold-text"><?php echo get_the_date(); ?></p>
							</div> <!-- END recent-post-content -->
						</div> <!-- END recent-post-background -->
					</a>

					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif;?>
		</div> <!-- END col col-lg-4 -->

	<div class="col-12 col-lg-3 col-no-pad">

			<?php if ($events_query->have_posts()) : while ($events_query->have_posts()) : $events_query->the_post(); ?>
				<a href="<?php echo the_permalink();?>">
					<div class="recent-post-background" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/handshake-grey.jpg'">
							<div class="recent-post-content">
								<h2>IWF EVENTS</h2>
								<h1><?php echo the_title(); ?></h1>
								<p class="gold-text"><?php echo get_the_date(); ?></p>
							</div> <!-- END recent-post-content -->
						</div> <!-- END recent-post-background -->
					</a>

					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif;?>
		</div> <!-- END col col-lg-4 -->

	</div>
</div>

<div class="container-fluid pt-5 vlg-my-4">
	<div class="row justify-content-center">
		<div class="col-xl-6 col-lg-8 col-md-10  text-md-center">
			<h2 class="mb-5">OUR WORK</h2>
			<h1><?php echo get_field('our_work_sub_heading'); ?></h1>
			<p class="mt-5"><?php echo get_field('our_work_paragraph'); ?> </p>

		</div>
	</div>
</div>

<div class="container-fluid has-wave" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/yellow-wave.svg;">
	<div class="row justify-content-center row-barriers-icons">
		<div class="col-12 text-center">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-briefcase.svg" alt="" class="barriers-icons">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-feet.svg" alt="" class="barriers-icons">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-home.svg" alt="" class="barriers-icons">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-graduation.svg" alt="" class="barriers-icons">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-car.svg" alt="" class="barriers-icons">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-handshake.svg" alt="" class="barriers-icons">
		</div>


	</div>
	<div class="row">
		<div class="col-12 text-md-center">
			<a href="<?php echo get_home_url(); ?><?php echo get_field('our_work_cta_link'); ?>" class="btn btn-primary"><?php echo get_field('our_work_cta_button'); ?></a>
		</div>
	</div>
</div>



<div class="container py-5 my-5">
	<div class="row">
		<div class="col-12 text-md-center">
			<h2>DRIVING CHANGE</h2>
			<h1><?php echo get_field('driving_change_sub_heading'); ?></h1>

		</div>
	</div>
	<div class="row values justify-content-center my-5 pt-5">
		<div class="col-sm-4 my-5 d-flex d-md-block align-items-center">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-one.svg" alt="">
			<div class="values-content">
				<h2><?php echo get_field('driving_change_one_heading'); ?></h2>
				<a href="<?php echo get_home_url(); ?><?php echo get_field('driving_change_one_cta_link'); ?>"><?php echo get_field('driving_change_one_cta_text'); ?></a>
			</div>
		</div>
		<div class="col-sm-4 my-5 d-flex d-md-block align-items-center">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-two.svg" alt="">
			<div class="values-content">
				<h2><?php echo get_field('driving_change_two_heading'); ?></h2>
				<a href="<?php echo get_home_url(); ?><?php echo get_field('driving_change_two_cta_link'); ?>"><?php echo get_field('driving_change_two_cta_text'); ?></a>
			</div>
		</div>
		<div class="col-sm-4 my-5 d-flex d-md-block align-items-center">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-three.svg" alt="">
			<div class="values-content">
				<h2><?php echo get_field('driving_change_three_heading'); ?></h2>
				<a href="<?php echo get_home_url(); ?><?php echo get_field('driving_change_three_cta_link'); ?>"><?php echo get_field('driving_change_three_cta_text'); ?></a>
			</div>
		</div>
	</div>

	<div class="row values justify-content-center my-5 pb-5">
		<div class="col-sm-4 my-5 d-flex d-md-block align-items-center">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-four.svg" alt="">
			<div class="values-content">
				<h2><?php echo get_field('driving_change_four_heading'); ?></h2>
				<a href="<?php echo get_home_url(); ?><?php echo get_field('driving_change_four_cta_link'); ?>"><?php echo get_field('driving_change_four_cta_text'); ?></a>
			</div>
		</div>
		<div class="col-sm-4 my-5 d-flex d-md-block align-items-center">
			<img src="<?php echo get_template_directory_uri(); ?>/images/icon-five.svg" alt="">
			<div class="values-content">
				<h2><?php echo get_field('driving_change_five_heading'); ?></h2>
				<a href="<?php echo get_home_url(); ?><?php echo get_field('driving_change_five_cta_link'); ?>"><?php echo get_field('driving_change_five_cta_text'); ?></a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<hr>
		</div>

	</div>
</div>

<div class="container py-5 my-5" id="newsletter">
	<div class="row justify-content-center">
		<div class="col-12 col-lg-9 text-md-center">
			<h2 class="mb-5">SUBSCRIBE TO OUR NEWSLETTER</h2>
			<h1>Stay connected and informed. Join our email list!</h1>
			<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>

		</div>
	</div>
</div>

<!-- MODAL -->
<div class="modal" id="videoModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-large" role="document">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			<iframe width="800" height="500" src="https://www.youtube.com/embed/oufy0-yMHIk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>

		</div>
	</div>
</div>
<!-- END MODAL -->






			</div> <!-- end #content -->

<?php get_footer(); ?>
