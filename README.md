IAWF website
===================


Getting started
__________

This theme uses SCSS to compile the CSS. To get started you need to setup a local environment (Local by Flywheel is recommended) and pull down the repo into the theme folder.

Run npm install.

Run npm run scss.
