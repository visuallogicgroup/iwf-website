<nav class="top-bar navbar navbar-expand-lg">

<?php
    wp_nav_menu( array(
      'theme_location'  => 'topbar',
      'depth'	          => 1, // 1 = no dropdowns, 2 = with dropdowns.
      'container'       => '',
      'container_class' => '',
      'container_id'    => '',
      'menu_class'      => 'navbar-nav ml-auto',
      'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
      'walker'          => new WP_Bootstrap_Navwalker(),
    ) ); ?>


    </nav>


<nav id="main-nav" class="navbar navbar-expand-lg">
  <a class="navbar-brand" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri();?>/images/logo.svg" alt=""></a>
  <!-- Method inspired by https://braveux.com/ -->

    <div class="mobile-menu-toggle-wrap d-block d-lg-none">
      <a href="javascript():void" class="mobile-menu-toggle">
        <span class="mobile-menu-box">
          <span class="mobile-menu-line"></span>
        </span>
      </a>
    </div>


  <?php
    wp_nav_menu( array(
      'theme_location'  => 'primary',
      'depth'	          => 2, // 1 = no dropdowns, 2 = with dropdowns.
      'container'       => 'div',
      'container_class' => 'collapse navbar-collapse',
      'container_id'    => 'navbarSupportedContent',
      'menu_class'      => 'navbar-nav ml-auto',
      'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
      'walker'          => new WP_Bootstrap_Navwalker(),
    ) ); ?>



</nav>
