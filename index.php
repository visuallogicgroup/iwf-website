<?php get_header(); ?>
			<div class="clearfix row-fluid homepage-image">
			<div class="featured-image">
			<?php $rand = rand(1, 4); ?>
			<div class="white-border" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/homepage-girl<?php echo $rand; ?>.jpg);">
					<div class="red-line"></div>
			</div>
			<div class="what-if-statement">
				<div class="header-container">
					<h1 class="header-statement active">What <i>if</i></h1>
					<h1 class="header-statement">Did <i>you</i> know</h1>
				</div>
				<div class="statement-container">
					<p class="statement active">women in Iowa led the nation in rate of business ownership?</p>
					<p class="statement">Iowa currently ranks 49th in the nation for number of women-owned businesses.  This number has decreased in the last decade.</p>
					<p class="statement">more Iowan women were empowered with the skills and confidence to run for public office?</p>
					<p class="statement">Iowa is one of two states to have never elected a female governor, US Senator, or US Representative.</p>
					<p class="statement">all of Iowa’s women and girls could be assured of security in their home lives?</p>
					<p class="statement">Among homeless families in Iowa, 85% are headed by women.</p>
				</div>
			</div>
</div>
			</div>

			<div id="content" class="row-fluid">

				<div id="main" class="span8">

					<h1 class="main-headline">The Iowa Women's Foundation</h1>
					<p style="margin-bottom: 40px;">is committed to improving the lives of Iowa's women and girls through economic self-sufficiency. With a diversified mix of action and funding including research, grantmaking, advocacy, education, and collaboration we work to alleviate the causes and not just the symptoms of economic illiteracy.</p>
<img src="<?php echo get_template_directory_uri(); ?>/images/two-column-divider.png" style="margin-bottom: 40px;"/>
				<div id="two-column" class="row-fluid">

					<div class="span6" id="set-apart">
						<h2 class="h3"><a href="#" rel="bookmark" title="The Iowa Women's Foundation">What sets us apart?</a></h2>
					<p>Our focus: we use research to uncover the biggest barriers to women's success as well as their greatest needs, and we invest our passion and resources accordingly. </p>
					</div><!-- /span6 -->
					<div class="span6" id="whats-happening">
						<h2 class="h3"><a href="./whats-happening/" rel="bookmark" title="The Iowa Women's Foundation">What's Happening</a></h2>
					<?php
	$args = array( 'category' => '4', 'numberposts' => '2' );
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent ){
		echo date( 'M d Y', strtotime( $recent['post_date'] ) );
		echo '<h4 style="font-size: 24px; margin-top: 0px; color: #5f2968; border-bottom: 1px solid #E4E4E4;padding-bottom: 7px;">';
		echo '<a style="font-size: 20px; font-weight: bold; color: #5f2968;" href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a>';
		echo '</h4>';
		echo '<p style="font-size: 16px; margin-bottom: 20px;">';
		echo  substr(strip_tags($recent["post_content"], '<br>'), 0 ,250).' ...';
		echo '<br /></p>';
	}
?>

					</div><!-- / #whats-happening .span6 -->
				</div><!-- / #two-column .row-fluid -->
			</div> <!-- end #main -->




				<div class="span4" role="main">
<div class="row-fluid">
			<div id="sidebar1" style="" class="fluid-sidebar sidebar" role="complementary">
				<div id="pssubpages-2" class="widget widget_pssubpages">
					<h4 class="widgettitle">How can you help?</h4>
					Make a contribution today.
					<div class="action-button">
						<a href="/your-support/donate/">Donate Now</a>
					</div>
					<div style="margin: 20px 0px 10px;" ><img src="/wp-content/themes/iawf/images/sidebar-divider.png" /></div>
					Help us support women and girls!
					<div class="action-button">
						<a href="/your-support/volunteer/">Volunteer</a>
					</div>
				</div>
			</div>
			<div><img style="width: 100%;" src="/wp-content/themes/iawf/images/sidebar-bar.png" /></div>
			<?php echo do_shortcode("[gravityform id='1' title='true' description='false' ajax='true']"); ?>
			<div id="sidebar2" class="fluid-sidebar sidebar" role="complementary">
				<div id="pssubpages-2" class="widget widget_pssubpages">
					Be a part of the solution.
					<div class="action-button apply-for-grant">
						<a href="/grantmaking/application/">Apply for a Grant</a>
					</div>
				</div>


			</div>
			
			
			
			
			<div id="gform_widget-2" class="widget gform_widget"><link rel="stylesheet" id="gforms_reset_css-css" href="/wp-content/plugins/gravityforms/css/formreset.css?ver=1.7.6" type="text/css" media="all">
<link rel="stylesheet" id="gforms_datepicker_css-css" href="/wp-content/plugins/gravityforms/css/datepicker.css?ver=1.7.6" type="text/css" media="all">
<link rel="stylesheet" id="gforms_formsmain_css-css" href="/wp-content/plugins/gravityforms/css/formsmain.css?ver=1.7.6" type="text/css" media="all">
<link rel="stylesheet" id="gforms_ready_class_css-css" href="/wp-content/plugins/gravityforms/css/readyclass.css?ver=1.7.6" type="text/css" media="all">
<link rel="stylesheet" id="gforms_browsers_css-css" href="/wp-content/plugins/gravityforms/css/browsers.css?ver=1.7.6" type="text/css" media="all">

                <div class="gf_browser_chrome gform_wrapper email-signup_wrapper" id="gform_wrapper_1">
                <p class="form-description">Stay connected and informed. Join our email list.</p>
                <a id="gf_1" name="gf_1" class="gform_anchor"></a><form method="post" enctype="multipart/form-data" target="gform_ajax_frame_1" id="gform_1" class="email-signup" action="/about-us/mission-vision/#gf_1">
                        <div class="gform_body">
                            <ul id="gform_fields_1" class="gform_fields top_label description_below"><li id="field_1_1" class="gfield               gfield_contains_required"><label class="gfield_label" for="input_1_1_3">Name<span class="gfield_required">*</span></label><div class="ginput_complex ginput_container" id="input_1_1"><span id="input_1_1_3_container" class="ginput_left"><input type="text" name="input_1.3" id="input_1_1_3" value="" tabindex="1"><label for="input_1_1_3">First</label></span><span id="input_1_1_6_container" class="ginput_right"><input type="text" name="input_1.6" id="input_1_1_6" value="" tabindex="2"><label for="input_1_1_6">Last</label></span><div class="gf_clear gf_clear_complex"></div></div></li><li id="field_1_2" class="gfield               gfield_contains_required"><label class="gfield_label" for="input_1_2">Email<span class="gfield_required">*</span></label><div class="ginput_container"><input name="input_2" id="input_1_2" type="email" value="" class="large" tabindex="3"></div></li><li id="field_1_3" class="gfield    gform_validation_container"><label class="gfield_label" for="input_1_3">Comments</label><div class="ginput_container"><input name="input_3" id="input_1_3" type="text" value="" autocomplete="off"></div><div class="gfield_description">This field is for validation purposes and should be left unchanged.</div></li>
                            </ul></div>
        <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_1" class="button gform_button" value="Sign up!" tabindex="4"><input type="hidden" name="gform_ajax" value="form_id=1&amp;title=&amp;description=">
            <input type="hidden" class="gform_hidden" name="is_submit_1" value="1">
            <input type="hidden" class="gform_hidden" name="gform_submit" value="1">
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
            <input type="hidden" class="gform_hidden" name="state_1" value="WyJhOjA6e30iLCI5MzVmNDgzZmI5MzU5Njc2MTQ1N2M1YmRkMDk0ZGQyOCJd">
            <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0">
            <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1">
            <input type="hidden" name="gform_field_values" value="">

        </div>
                </form>
                </div>
                <iframe style="display:none;width:0px; height:0px;" src="about:blank" name="gform_ajax_frame_1" id="gform_ajax_frame_1"></iframe>
                <script type="text/javascript">function gformInitSpinner_1(){jQuery('#gform_1').submit(function(){if(jQuery('#gform_ajax_spinner_1').length == 0){jQuery('#gform_submit_button_1, #gform_wrapper_1 .gform_previous_button, #gform_wrapper_1 .gform_next_button, #gform_wrapper_1 .gform_image_button').attr('disabled', true); jQuery('#gform_submit_button_1, #gform_wrapper_1 .gform_next_button, #gform_wrapper_1 .gform_image_button').after('<' + 'img id="gform_ajax_spinner_1"  class="gform_ajax_spinner" src="http://iawf.vlgux.com/wp-content/plugins/gravityforms/images/spinner.gif" alt="" />'); }} );}jQuery(document).ready(function($){gformInitSpinner_1();jQuery('#gform_ajax_frame_1').load( function(){var contents = jQuery(this).contents().find('*').html();var is_postback = contents.indexOf('GF_AJAX_POSTBACK') >= 0;if(!is_postback){return;}var form_content = jQuery(this).contents().find('#gform_wrapper_1');var is_redirect = contents.indexOf('gformRedirect(){') >= 0;jQuery('#gform_submit_button_1').removeAttr('disabled');var is_form = !(form_content.length <= 0 || is_redirect);if(is_form){jQuery('#gform_wrapper_1').html(form_content.html());jQuery(document).scrollTop(jQuery('#gform_wrapper_1').offset().top);if(window['gformInitDatepicker']) {gformInitDatepicker();}if(window['gformInitPriceFields']) {gformInitPriceFields();}var current_page = jQuery('#gform_source_page_number_1').val();gformInitSpinner_1();jQuery(document).trigger('gform_page_loaded', [1, current_page]);}else if(!is_redirect){var confirmation_content = jQuery(this).contents().find('#gforms_confirmation_message').html();if(!confirmation_content){confirmation_content = contents;}setTimeout(function(){jQuery('#gform_wrapper_1').replaceWith('<' + 'div id=\'gforms_confirmation_message\' class=\'gform_confirmation_message_1\'' + '>' + confirmation_content + '<' + '/div' + '>');jQuery(document).scrollTop(jQuery('#gforms_confirmation_message').offset().top);jQuery(document).trigger('gform_confirmation_loaded', [1]);}, 50);}else{jQuery('#gform_1').append(contents);if(window['gformRedirect']) {gformRedirect();}}jQuery(document).trigger('gform_post_render', [1, current_page]);} );} );</script><script type="text/javascript"> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); </script></div>

							</div>
			</div>

			</div> <!-- end #content -->
<script>
jQuery(document).ready(function ($) {
	function fadeStatement($ele) {
	    $ele.fadeIn(2000).delay(5000).fadeOut(2000, function() {
	        var $next = $(this).next('p.statement');
	        // if there is a following `p` element, it is faded in
	        // otherwise start from the beginning by taking
	        // the parent's first child
	        fadeStatement($next.length > 0 ? $next : $(this).parent().children().first());
	   });
	};

	function fadeHeader($ele) {
	    $ele.fadeIn(2000).delay(5000).fadeOut(2000, function() {
	        var $next = $(this).next('h1.header-statement');
	        // if there is a following `p` element, it is faded in
	        // otherwise start from the beginning by taking
	        // the parent's first child
	        fadeHeader($next.length > 0 ? $next : $(this).parent().children().first());
	   });
	};

	fadeStatement($('p.statement').first());
	fadeHeader($('h1.header-statement').first());
});
</script>
<?php get_footer(); ?>
