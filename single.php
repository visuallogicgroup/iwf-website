<?php get_header(); ?>


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<header>

			<div class="page-header">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-8 col-12">
							<h1 class="single-title" itemprop="headline"><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>

	</header> <!-- end article header -->

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8 col-12">


	<p class="meta"><time datetime="<?php echo the_time('Y-m-j'); ?>" style="font-size:1.3rem;" pubdate><em><?php the_date(); ?></em></time></p>
		<article id="post-<?php the_ID(); ?>" role="article" itemscope itemtype="http://schema.org/BlogPosting">



			<section class="post_content" itemprop="articleBody">

				<?php the_content(); ?>

				<?php wp_link_pages(); ?>


			</section> <!-- end article section -->

			<footer>

				<?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","bonestheme") . ':</span> ', ' ', '</p>'); ?>

					<?php
						// only show edit button if user has permission to edit posts
						if( $user_level > 0 ) {
						?>
						<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post">
							<i class="icon-pencil icon-white"></i> <?php _e("Edit post","bonestheme"); ?>
						</a>
					<?php } ?>

			</footer> <!-- end article footer -->

		</article> <!-- end article -->

	<?php endwhile; ?>

	<?php else : ?>

		<article id="post-not-found">
				<header>
					<h1><?php _e("Not Found", "bonestheme"); ?></h1>
				</header>
				<section class="post_content">
					<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
				</section>
		</article>

	<?php endif; ?>

		</div>
	</div>
</div>

<?php get_footer(); ?>
