<?php
/*
Template Name: Donate Page
*/
?>

<?php get_header(); ?>





					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<header>
							<div class="page-header">
								<h1><?php the_title(); ?></h1>
							</div>
						</header> <!-- end article header -->
						<div class="container">
							<div class="row justify-content-center mb-5">
								<div class="col-md-12">
									<div class="row donate-boxes align-items-stretch">
										<div class="col-lg-4">
											<div class="donate-box h-100 mb-4">
												<h3><a href="/donate-now/">Make an Online Contribution</a></h3>
												<p>An online contribution is the easiest way to give to the Iowa Women’s Foundation. You do not need to have a PayPal account and all major credit cards are accepted.</p>
												<p><a class="btn btn-primary btn-block btn-large" href="/donate-now/">Donate Now</a></p>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="donate-box h-100 mb-4">
												<h3><a href="/child-care-solutions-fund-donation/">Support the Child Care Solutions Fund</a></h3>
												<p>Investments in the Child Care Solutions Fund focus on strategies to increase women's economic security by decreasing the workforce gap through access to quality, affordable child care.</p>
												<p><a class="btn btn-primary btn-block btn-large" href="/child-care-solutions-fund-donation/">Donate Now</a></p>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="donate-box h-100 mb-4">
												<h3><a href="http://iawf.org/whats-happening/events/#ovation">Honor a Special Woman</a></h3>
												<p>Honor an extraordinary woman in Iowa and improve the lives of all Iowa’s women and girls by submitting a tribute to "Ovation", our annual publication.</p>
												<p><a class="btn btn-primary btn-block btn-large" href="/tribute-publication-commitment/">Get started</a></p>
											</div>
										</div>



									</div>
								</div>
							</div>

							<div class="row justify-content-center">
								<div class="col-md-12 col-lg-8 col-12">

									<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
									<section class="post_content">



									<div style="text-align:center;">
										<h3>Other Ways to Donate</h3>
										<hr class="two-column-divider" style="background-position:center;">
									</div>

									<?php the_content(); ?>

								</section> <!-- end article section -->


								</article> <!-- end article -->

								</div>
							</div>
						</div> <!-- END .container -->


					<style>
						.donate-boxes {
							margin-left:0!important;
						}
						.donate-box {
							background-color:#f0ece9;
							padding:30px;
							min-height: 285px;
							margin-bottom:30px;
						}
						.donate-box p {
							margin-bottom:30px;
						}

					</style>


					<?php endwhile; ?>

					<?php else : ?>

					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>

					<?php endif; ?>



<?php get_footer(); ?>
