<?php get_header(); ?>
			
<header class="page-header">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 col-12">
					<h1>Error: 404</h1>
				</div>
			</div>
		</div>
	</header>

	<div id="main" class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8 col-12">
				<h1><?php _e("Epic 404 - Article Not Found","bonestheme"); ?></h1>
				<p><?php _e("This is embarassing. We can't find what you were looking for.","bonestheme"); ?></p>
				<p><?php _e("Whatever you were looking for was not found, but maybe try looking again or search using the form below.","bonestheme"); ?></p>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-lg-8 col-12">
				<?php get_search_form(); ?>
			</div>
		</div>				


	</div>

			

<?php get_footer(); ?>
